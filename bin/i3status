#!/usr/bin/env ruby

require 'date'
require 'json'

def get_brightness_percent
	curr = IO.read '/sys/class/backlight/amdgpu_bl0/actual_brightness'
	max = 65535  # grrr
	(curr.to_f * 100/ max.to_f).round
end

def get_volume_percent
	`~/bin/volume`
end

def date_pretty
	now = DateTime.now  #.new_offset(0)
	#return DateTime.now.new_offset(0).iso8601.sub '+00:00', 'Z'
	case now.offset
	when 0
		now.strftime '%Y-%m-%d %H:%M:%S'
	else
		now.strftime '%Y-%m-%d "%H:%M:%S"'
	end
end

def get_load_1m
	loads = IO.read '/proc/loadavg'
	loads.split(' ',2)[0]
end

def get_ip(if_name)
	ips = `ip -o -f inet addr show primary up`
	ipline = ips.each_line .find {|l| l.match /^\d+: #{if_name}/}
	(ipline.split(' inet ',2)[1]).split('/',2)[0]
end

def wlan_pretty(if_name)
	return 'off' if `rfkill list wifi`.include? 'yes'
	ip = get_ip(if_name)
	wstat = IO.read '/proc/net/wireless'
	stats = wstat.each_line .find {|l| l.match /^#{if_name}/} .split /\s+/
	"#{stats[3].chomp('.')}dB #{stats[5..10].map(&:to_i).inject(:+)}! #{ip}"
end

def eth_pretty(*if_names)
	ips = if_names.map{|if_name| get_ip(if_name) rescue nil }.compact
	raise if ips.empty?
	ips.join(' ')
end

def disk_space
	`df -h /`.lines()[1].split(/\s+/)[3]
end

def memory_space
	lines = IO.read('/proc/meminfo').each_line.to_a
	swap_free = lines.find {|l| l.match /^SwapFree:/ }.split(/\s+/)[1].to_f
	mem_avail = lines.find {|l| l.match /^MemAvailable:/ }.split(/\s+/)[1].to_f
	"#{(mem_avail / 1024 / 1024).round(1)}G #{(swap_free / 1024 / 1024).round(1)}G"
end

def battery_stuff(whichbat, label=nil)
	begin
		designed_cap = IO.read("/sys/class/power_supply/#{whichbat}/energy_full_design").chomp.to_f
		actual_cap = IO.read("/sys/class/power_supply/#{whichbat}/energy_full").chomp.to_f
		current = IO.read("/sys/class/power_supply/#{whichbat}/energy_now").chomp.to_f
		status = IO.read("/sys/class/power_supply/#{whichbat}/status").chomp

		percent = (100 * current / designed_cap).round(1)

		color = case
			when (status == 'Charging' || status == 'Unknown') then '#dddd00'
			when percent <= 5 then '#ff0000'
			when (status == 'Full' || status == 'Unknown') && current >= (actual_cap * 0.97) then '#009900'
			else '#999999'
		end

		{ name: :battery, instance: whichbat, color: color, full_text: "#{label ? (label + ' ') : ''}#{percent}" }
	rescue
		{ name: :battery, instance: whichbat, color: '#ff0000', full_text: "#{label ? (label + ' ') : ''}X" }
	end
end

def battery_summary
	bats = ['/sys/class/power_supply/BAT0', '/sys/class/power_supply/BAT1']
	ac = IO.read('/sys/class/power_supply/AC/online').to_i != 0
	energy = bats.map {|b| IO.read("#{b}/energy_now").to_i }.inject(:+).to_f
	power = bats.map {|b| IO.read("#{b}/power_now").to_i }.inject(:+).to_f
	energy = bats.map {|b| IO.read("#{b}/energy_full").to_i }.inject(:+).to_f - energy if ac
	run_out = DateTime.now + (energy/power/24)
	hours = (energy / power)
	"#{hours.to_i}h#{((hours*60)%60).to_i}m"
	#hours.round(1).to_s
	#run_out.strftime '%H:%M'
end

def timer
	return nil unless File.exist? '/dev/shm/tom-time'
	stuff = { name: :timer, color: "#aa8800" }
	target_time = DateTime.strptime(File::read('/dev/shm/tom-time'))
	distance = target_time - DateTime.now
	if distance < 0
		stuff['full_text'] = target_time.iso8601
		stuff['color'] = '#330000'
		stuff['background'] = '#ff0000'
		stuff['border'] = '#ff0000'
		stuff['urgent'] = true
	else
		if (distance*24*60*60).to_i <= 60
			stuff['full_text'] = "#{(distance*24*60*60).to_i}s"
		else
			stuff['full_text'] = "#{(distance*24*60).to_i}m"
		end
	end
	stuff
end

def ssh_agent
	keys = `ssh-add -l`
	return nil if $?.exitstatus == 2
	count = keys.lines.length if $?.exitstatus == 0
	color = $?.exitstatus == 0 ? '#22dd66' : '#dd4422'
	instance = $?.exitstatus == 0 ? 'keyed' : 'unkeyed'
	{ name: :ssh_agent, instance: instance, color: color, full_text: "#{count}🔑" }
end

def get_output_string
	stuff = []
	stuff << { name: :disk_info, instance: '/', color: '#666666', full_text: disk_space }
	stuff << { name: :mem_info, color: '#666666', full_text: memory_space }
	stuff.last.merge!({separator: true})

	#$stderr.puts `bluetoothctl <<< 'show 5C:E0:C5:02:6B:B4' | grep Powered`
	#if `bluetoothctl <<< 'show 5C:E0:C5:02:6B:B4' | grep Powered`.include? 'yes'
	#	stuff << ({name: :bluetooth, instance: :enabled, color: '#6677cc', full_text: 'BT'})
	#else
	#	stuff << ({name: :bluetooth, instance: :disabled, color: '#444444', full_text: 'BT'})
	#end
	stuff << ( { name: :ethernet, instance: 'tun0', color: '#ffff00', full_text: "VPN: #{eth_pretty('tun0', 'wg-velo', 'wg-home')}" } rescue { name: :ethernet, instance: 'tun0', color: '#444444', full_text: 'VPN: down' } )
	stuff << ( { name: :ethernet, instance: 'enp3s0f0', color: '#6677cc', full_text: "E: #{eth_pretty('enp3s0f0', 'enp4s0')}" } rescue { name: :ethernet, instance: 'enp3s0f0', color: '#444444', full_text: 'E: down' } )
	begin
		wlan = wlan_pretty 'wlp1s0'
		stuff <<  { name: :wireless, instance: 'wlp1s0', color: (wlan=='off' ? '#444444' : '#6677cc'), full_text: "W: #{wlan}" }
	rescue
		stuff << { name: :wireless, instance: 'wlp3s0', color: '#880000', full_text: 'W: down' }
	end
	stuff.last.merge!({separator: true})

	stuff << { name: :ac, instance: 'ac', color: File.read('/sys/class/power_supply/AC/online').to_i == 1 ? '#00ff00' : '#880000', full_text: 'ac'||'🗲' }
	stuff << battery_stuff('BAT0')
	stuff << {name: :battery_summary, full_text: battery_summary } rescue nil
	stuff.last.merge!({separator: true})

	stuff << { name: :load, full_text: get_load_1m }
	stuff.last.merge!({separator: true})

	stuff << { name: :brightness, full_text: "☼ #{get_brightness_percent}%" }
	volicon = case
		when (@cached_volume_percent.include? 'Dock') then '📺' + ((@cached_volume_percent.include? 'Mute') ? "\u0338" : '')
		when (@cached_volume_percent.include? 'Headphone') then '🎧' + ((@cached_volume_percent.include? 'Mute') ? "\u0338" : '')
		when @cached_volume_percent.to_i > 50 then '🔊'
		when @cached_volume_percent.to_i > 0 && !(@cached_volume_percent.include? 'Mute') then '🔉'
		else '🔇'
	end
	stuff << { name: :volume, full_text: "#{volicon} #{@cached_volume_percent.split("\n")[0]}" }
	#stuff << { name: :volume, full_text: "#{volicon} #{@cached_volume_percent.split("\n")[0]} #{@cached_dev_changes}" }
	stuff.last.merge!({separator: true})
	stuff.last.merge!({color: '#dd8800'}) if @cached_volume_percent.include? 'Mute'

	stuff << { name: :tztime, instance: :local, full_text: date_pretty }
	stuff << timer if timer
	stuff << ssh_agent if ssh_agent
	stuff << { name: :caps, full_text: "CAPS", background: '#aa0000' } if @capslock

	stuff.each do |item|
		if item[:separator]
			item[:separator_block_width] = 27
		else
			item[:separator] = false
			item[:separator_block_width] = 13
		end
	end

	return JSON.generate(stuff)
end

@cached_dev_changes = 0
@cached_volume_percent = get_volume_percent
@capslock = false

Thread.abort_on_exception = true

redraw = Thread.new do
	$stdout.syswrite "{\"version\":1, \"click_events\":true}\n"
	$stdout.syswrite "[\n"
	first = true
	loop do
		$stdout.syswrite "#{first ? '' : ','}#{get_output_string}\n"
		first = false
		t = Time.now
		sleep (5 - (t.sec + t.subsec) % 5)
	end
end

receive_click_events = Thread.new do
loop do
	begin
		click = $stdin.gets
		click = click[1..-1] if click.start_with? ','
		$stderr.puts click
		click = JSON.parse(click)

		if click['button'] == 3 && (click['name'] == 'wireless' || click['name'] == 'ethernet')
			p = Process.spawn("echo #{get_ip click['instance']} | xclip -selection clipboard || true")
			Process.detach(p)
		elsif click['button'] == 1 && click['name'] == 'bluetooth'
			$stderr.puts "bluetoothctl <<< 'power #{click['instance']!='enabled' ? 'on' : 'off'}' | cat"
			p = `bluetoothctl <<< 'power #{click['instance']!='enabled' ? 'on' : 'off'}' | cat`
			$stderr.puts p
		elsif click['button'] == 1 && click['name'] == 'tztime'
			p = Process.spawn(%q{terminator --geometry='560x440' -bm --title='Calendar' --role=calendar -e 'bash -c "echo -ne \"\\033]0;Calendar\\007\"; cal -n 9 -S; read -n 1"'})
			Process.detach(p)
		elsif click['button'] == 3 && click['name'] == 'tztime'
			now = DateTime.now.new_offset(0)
			p = Process.spawn("echo #{now.strftime('%Y-%m-%dT%H:%M:%SZ')} | xclip -selection clipboard || true")
			Process.detach(p)
		elsif click['name'] == 'volume'
			if click['button'] == 4
				`~/bin/volume up`
			elsif click['button'] == 5
				`~/bin/volume down`
			end
		elsif click['name'] == 'brightness'
			if click['button'] == 4
				`~/bin/brightness up`
			elsif click['button'] == 5
				`~/bin/brightness down`
			end
		end
	rescue
	end
end
end

Signal.trap 'USR1' do
	redraw.run
end

$alsa_dev_pid = nil
at_exit { $alsa_dev_pid && Process.kill('TERM', $alsa_dev_pid) }
receive_alsa_dev_events = Thread.new do
loop do
	IO.popen "inotifywait -m /dev/snd -e create -e delete" do |io|
		$alsa_dev_pid = io.pid
		Process.detach($alsa_dev_pid)
		io.each do |line|
			@cached_dev_changes += 1
			redraw.run
		end
	end
end
end

$alsa_pid = nil
at_exit { $alsa_pid && Process.kill('TERM', $alsa_pid) }
receive_alsa_events = Thread.new do
loop do
	IO.popen "stdbuf -oL alsactl monitor" do |io|
		$alsa_pid = io.pid
		Process.detach($alsa_pid)
		io.each do |line|
			@cached_volume_percent = get_volume_percent  # TODO deduplicate (left+right?) events
			redraw.run
		end
	end
end
end

$xkb_pid = nil
at_exit { $xkb_pid && Process.kill('TERM', $xkb_pid) }
receive_xkb_events = Thread.new do
loop do
	IO.popen "stdbuf -oL xkbevd" do |io|
		$xkb_pid = io.pid
		Process.detach($xkb_pid)
		watching = false
		io.each do |line|
			if watching
				@capslock = line.include? 'new state= 0x00000001'
				watching = false
				redraw.run
			else
				watching = true if line.include? 'XkbIndicatorStateNotify'
			end
		end
	end
end
end

receive_alsa_events.join
receive_alsa_dev_events.join
receive_xkb_events.join
receive_click_events.join
redraw.join
